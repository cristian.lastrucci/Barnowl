Barnowl è un sistema distribuito che utilizza principi, algoritmi e tecnologie
comuni nell’ambito <i>Big data</i>, per ricevere ed elaborare, in tempo reale, 
messaggi geolocalizzati provenienti da Twitter, al fine di conoscere gli 
argomenti di discussione più frequenti, in un insieme predefinito di città nel mondo.<br />

In particolare la struttura del sistema sviluppato ricalca la <b>Lambda Architecture</b>,
con l’intento da una parte di implementare un meccanismo di elaborazione dei dati
in grado di essere distribuito su un cluster di macchine, e dall’altra di fornire 
i risultati in tempo reale, senza dover attendere il termine dell’elaborazione
per conoscere le statistiche desiderate (altrimenti da considerarsi obsolete già
durante l’esecuzione dell’analisi a causa della ricezione di nuovi dati).<br />

Per rendere possibile la parallelizzazione dell’analisi dei tweets Barnowl 
utilizza una specifica implementazione del modello <b>MapReduce</b>, sebbene
il prototipo realizzato sia stato testato su un cluster composto da un unico nodo.<br />

Barnowl è inoltre fortemente integrato all’interno dell’ecosistema di <b>Apache Hadoop</b>,
del quale sfrutta numerosi servizi per quanto riguarda la creazione del canale 
per l’acquisizione dei tweets (Apache Flume), la conservazione e l’interrogazione
dei dati in ingresso e delle statistiche generate (HDFS e HBase) e per la stessa
implementazione del modello MapReduce.<br />

I risultati dell’elaborazione dei tweets, in continua evoluzione, sono resi disponili
mediante una pagina web implementata con <b>React</b>, che comunica con il backend
mediante l’invocazione di un set di servizi REST.<br />

Per visualizzare la presentazione del progetto clicca [qui](/docs/barnowl-presentation.pdf), 
mentre per visualizzare la relazione completa clicca [qui](/docs/barnowl-relation.pdf).<br /><br />
