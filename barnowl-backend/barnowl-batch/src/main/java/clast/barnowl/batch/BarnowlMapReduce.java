package clast.barnowl.batch;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import clast.barnowl.model.SpeedNewKeyword;
import clast.barnowl.model.SpeedOldKeyword;
import clast.barnowl.model.Utils;

public class BarnowlMapReduce extends Configured implements Tool {

	private static Logger logger = LoggerFactory.getLogger(BarnowlMapReduce.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hbase_pu");
	private static EntityManager em = emf.createEntityManager();

	public static void main(String[] args) throws Exception {

		Configuration conf = HBaseConfiguration.create();
		int cycle = 0;
		
		emptyHDFSFolder(conf, new Path(args[0]), "temporarySource");
		
		truncateHbaseTable( "SpeedNewKeyword", "speedNew" );
		truncateHbaseTable( "SpeedOldKeyword", "speedOld" );
		truncateHbaseTable( "Batch1Keyword", "batch1" );
		truncateHbaseTable( "Batch2Keyword", "batch2" );
		truncateHbaseTable( "Utils", "utils" );

		do {

			logger.info("BARNOWL-BATCH: Batch cycle " + cycle + " started");

			try {
				batchCycleInitialization(conf, args, cycle);
			} catch (Exception e) {
				throw new Exception("Errors occured on batch cycle inizialization: " + e);
			}
			
			ToolRunner.run(conf, new BarnowlMapReduce(), addCycleToArgs(args, cycle) );

			logger.info("BARNOWL-BATCH: Batch cycle " + cycle + " ended");

			cycle++;

		} while ("true".equals(args[2]));

	}

	/** Svuota una cartella HDFS */
	private static void emptyHDFSFolder(Configuration conf, Path folder, String folderName) throws IOException {
		FileSystem fs = FileSystem.get(conf);
	
		RemoteIterator<LocatedFileStatus> fileStatusListIterator = fs.listFiles(folder, true);

		while (fileStatusListIterator.hasNext()) {
			LocatedFileStatus fileStatus = fileStatusListIterator.next();
			fs.delete(fileStatus.getPath(), false);
		}

		logger.info("BARNOWL-BATCH: Emptied " + folderName);
	}

	/** Svuota una tabella HBase*/
	private static void truncateHbaseTable( String tableClassLabel, String tableLabel ) {
		List<Object> rows;
		do {
			em.createQuery("delete from " + tableClassLabel + " k").executeUpdate();
			rows = em.createQuery("select k from " + tableClassLabel + " k").getResultList();
			em.clear();
		}while(rows != null && rows.size() > 0);
		logger.info("BARNOWL-BATCH: Truncated " + tableLabel);
	}

	private static String[] addCycleToArgs(String[] args, int cycle) {
		String[] newArgs = new String[4];
		newArgs[0] = args[0];
		newArgs[1] = args[1];
		newArgs[2] = args[2];
		newArgs[3] = String.valueOf(cycle);
		return newArgs;
	}

	/**
	 * Inizializza temporarySource, mainSource, speedNew, speedOld, batch1 e batch2
	 * all'avvio di ogni ciclo di batch
	 */
	private static void batchCycleInitialization(Configuration conf, String[] args, int cycle) throws IOException {
		logger.info("BARNOWL-BATCH: Batch cycle inizialization started");
		initSourceFolders(conf, args);
		initSpeedTables();
		initBatchTable(cycle);
		setQueryableBatchTable(cycle);
		logger.info("BARNOWL-BATCH: Batch cycle inizialization ended");
	}

	/** Sposta il contenuto di temporarySource in mainSource */
	private static void initSourceFolders(Configuration conf, String[] args) throws IOException {

		logger.info("BARNOWL-BATCH: Moving of temporarySource content in mainSource started");

		FileSystem fs = FileSystem.get(conf);
		Path temporarySource = new Path(args[0]);
		Path mainSource = new Path(args[1]);

		RemoteIterator<LocatedFileStatus> fileStatusListIterator = fs.listFiles(temporarySource, true);

		while (fileStatusListIterator.hasNext()) {
			LocatedFileStatus fileStatus = fileStatusListIterator.next();

			if (fileStatus.getPath().getName().endsWith(".tmp")) {
				logger.info("BARNOWL-BATCH: Open file " + fileStatus.getPath().getName() + " not moved to mainSource");
			} else {
				fs.rename(fileStatus.getPath(), mainSource);
				logger.info("BARNOWL-BATCH: Moved file " + fileStatus.getPath().getName() + " from temporarySource to mainSource");
			}
		}

		logger.info("BARNOWL-BATCH: Moving of temporarySource content in mainSource ended");
	}

	/** Svuota speedOld e ci sposta il contenuto di speedNew */
	private static void initSpeedTables() {

		List<SpeedNewKeyword> rows = null;

		truncateHbaseTable( "SpeedOldKeyword", "speedOld" );

		logger.info("BARNOWL-BATCH: Start move speedNew content in speedOld");
		do {

			rows = em.createQuery("select k from SpeedNewKeyword k").getResultList();

			for (SpeedNewKeyword row : rows) {

				SpeedOldKeyword keyword = new SpeedOldKeyword();
				keyword.setId(row.getId());
				keyword.setKeyword(row.getKeyword());
				keyword.setCity(row.getCity());
				keyword.setOccurrency(row.getOccurrency());
				em.persist(keyword);

				Query q = em.createQuery("delete from SpeedNewKeyword k where k.id = :uuid");
				q.setParameter("uuid", row.getId()).executeUpdate();

			}

			if (rows != null && rows.size() > 0) {
				logger.info("BARNOWL-BATCH: Moved " + rows.size() + " keywords from speedNew to speedOld");
			}

		} while (rows != null && rows.size() > 0);

		logger.info("BARNOWL-BATCH: End move speedNew content in speedOld");

	}

	/**
	 * Svuota la tabella di batch contenente i risultati del ciclo eseguito due
	 * iterazioni fa, che verrà impiegata nel ciclo corrente
	 */
	private static void initBatchTable(int cycle) {
		if (Integer.valueOf(cycle) % 2 == 0) {
			truncateHbaseTable( "Batch1Keyword", "batch1" );
		} else {
			truncateHbaseTable( "Batch2Keyword", "batch2" );
		}
	}
	
	/**
	 * Salva all'interno della tabella utils la tabella batch che deve essere
	 * utilizzata per le interrogazioni
	 */
	private static void setQueryableBatchTable(int cycle) {
		
		String queryableBatchTable;
		if( cycle < 2 || cycle % 2 == 1 ) {
			queryableBatchTable = "batch1";
		}else {
			queryableBatchTable = "batch2";
		}
		try {
			
			Utils utils = (Utils) em.createQuery("select u from Utils u").getSingleResult();
			Utils u = em.find(Utils.class, utils.getId());
			u.setCurrenBatchTable(queryableBatchTable);
			em.merge(u);
			
		}catch (NoResultException e) {
			
			Utils utils = new Utils();
			utils.setId( UUID.randomUUID() );
			utils.setCurrenBatchTable(queryableBatchTable);
			em.persist(utils);
		}

		logger.info("BARNOWL-BATCH: " + queryableBatchTable + " has been set as queryableBatchTable in utils");
	}

	public int run(String[] args) throws Exception {

		Configuration conf = HBaseConfiguration.create();

		Job job = Job.getInstance(conf, "BarnowlJob");

		job.setJarByClass(BarnowlMapReduce.class);

		job.setMapperClass(BarnowlMapper.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		FileInputFormat.setInputPaths(job, new Path(args[1]));

		if (Integer.valueOf(args[3]) % 2 == 0) {
			TableMapReduceUtil.initTableReducerJob("hbaseDB:batch1", BarnowlReducer1.class, job);
			job.setReducerClass(BarnowlReducer1.class);
		} else {
			TableMapReduceUtil.initTableReducerJob("hbaseDB:batch2", BarnowlReducer2.class, job);
			job.setReducerClass(BarnowlReducer2.class);
		}

		return job.waitForCompletion(true) ? 0 : 1;
	}

}
