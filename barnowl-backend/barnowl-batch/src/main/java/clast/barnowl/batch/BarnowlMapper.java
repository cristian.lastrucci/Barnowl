package clast.barnowl.batch;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import clast.barnowl.filters.StopWords;
import clast.barnowl.model.City;
import clast.barnowl.model.Coordinates;
import clast.barnowl.model.UrbanArea;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

public class BarnowlMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	private final static IntWritable one = new IntWritable(1);
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		
		String[] tweets = value.toString().split("\\n");
		JSONParser jsonParser = new JSONParser(JSONParser.MODE_PERMISSIVE);

		try {
			for (String jsonTweet : tweets) {

				JSONObject tweet = (JSONObject) jsonParser.parse(jsonTweet);

				String tweetText = (String) tweet.get("text");
				Object tweetLocation = tweet.get("coordinates");
				
				if(tweetText != null && tweetLocation != null) {
					
					Coordinates tweetCoords = new Coordinates();
					tweetCoords.initFromJSON( tweetLocation.toString() );
					
					City tweetCity = UrbanArea.findCity(tweetCoords);
					
					if(tweetCity != null) {
						
						
						String[] tweetWords = tweetText.toString().split(" ");
						
						for (String word : tweetWords) {
							if( !StopWords.isStopWord(word) ) {
								context.write(new Text(tweetCity.toString() + " - " + word.toLowerCase()), one);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}