package clast.barnowl.batch;

import java.io.IOException;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import clast.barnowl.model.Batch2Keyword;
import clast.barnowl.model.City;

public class BarnowlReducer2 extends TableReducer<Text, IntWritable, ImmutableBytesWritable> {
	
	private static Logger logger = LoggerFactory.getLogger(BarnowlMapReduce.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hbase_pu");
    private static EntityManager em = emf.createEntityManager();
	
	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		
		String[] k = key.toString().split(" - ");
		
		if(k.length == 2) {
			
			String city = k[0];
			String word = k[1];
			
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			String result = new Integer(sum).toString();   
			
			Batch2Keyword keyword = new Batch2Keyword();
			keyword.setId( UUID.randomUUID() );
			keyword.setKeyword( word );
			keyword.setCity( City.valueOf(city) );
			keyword.setOccurrency( sum );
			
			em.persist(keyword);
			
			logger.info( "BARNOWL-BATCH: Stored keyword in batch2: " + city + " - " + word + " - " + result);
		}
	}
}
