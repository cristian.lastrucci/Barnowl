package clast.barnowl.speed;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.conf.ComponentConfiguration;
import org.apache.flume.sink.hbase.AsyncHbaseEventSerializer;
import org.hbase.async.AtomicIncrementRequest;
import org.hbase.async.PutRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import clast.barnowl.filters.StopWords;
import clast.barnowl.model.City;
import clast.barnowl.model.Coordinates;
import clast.barnowl.model.SpeedNewKeyword;
import clast.barnowl.model.UrbanArea;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.json.DataObjectFactory;

public class HBaseSerializer implements AsyncHbaseEventSerializer {

	private static Logger logger;
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hbase_pu");
	private static EntityManager em = emf.createEntityManager();

	private Event currentEvent;

	@Override
	public void initialize(byte[] table, byte[] cf) {	}

	@Override
	public void setEvent(Event event) {
		this.currentEvent = event;
	}

	@Override
	public List<PutRequest> getActions() {

		String eventStr = new String(currentEvent.getBody());
		try {

			Status tweet = DataObjectFactory.createStatus(eventStr);

			if (tweet.getGeoLocation() != null && tweet.getText() != null) {

				Coordinates tweetCoords = new Coordinates(tweet.getGeoLocation().getLatitude(),
						tweet.getGeoLocation().getLongitude());
				City tweetCity = UrbanArea.findCity(tweetCoords);

				if (tweetCity != null) {

					String[] tweetWords = tweet.getText().toString().split(" ");

					for (String word : tweetWords) {
						if (!StopWords.isStopWord(word)) {

							SpeedNewKeyword keyword = new SpeedNewKeyword();
							keyword.setId( UUID.randomUUID() );
							keyword.setKeyword( word.toLowerCase() );
							keyword.setCity( tweetCity );
							keyword.setOccurrency( 1 );
							em.persist(keyword);
							
							logger.info("BARNOWL-SPEED: Stored keyword in speedNew: " + tweetCity + " - " + word);
						}
					}
				}
			}

		} catch (TwitterException ex) {
			throw new RuntimeException("Error parsing Twitter string");
		}

		return new ArrayList<PutRequest>();
	}

	@Override
	public List<AtomicIncrementRequest> getIncrements() {
		return new ArrayList<AtomicIncrementRequest>();
	}

	@Override
	public void cleanUp() {
		currentEvent = null;
	}

	@Override
	public void configure(Context context) {

		logger = LoggerFactory.getLogger(TwitterListener.class);

		String cols = new String(context.getString("columns"));
		String[] names = cols.split(",");
		byte[][] columnNames = new byte[names.length][];
		int i = 0;
		for (String name : names) {
			columnNames[i++] = name.getBytes();
		}
	}

	@Override
	public void configure(ComponentConfiguration conf) {}

}
