package clast.barnowl.speed;

import java.util.HashMap;
import java.util.Map;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

public class TwitterListener extends AbstractSource implements EventDrivenSource, Configurable {

	private static Logger logger;
	private TwitterStream stream;
	
	public void configure(Context context) {

		ConfigurationBuilder cb = new ConfigurationBuilder();
		
		cb.setDebugEnabled(true)
			.setOAuthConsumerKey( TwitterListenerUtils.CONSUMER_KEY )
			.setOAuthConsumerSecret( TwitterListenerUtils.CONSUMER_SECRET )
			.setOAuthAccessToken( TwitterListenerUtils.ACCESS_TOKEN )
			.setOAuthAccessTokenSecret( TwitterListenerUtils.ACCESS_TOKEN_SECRET )
			.setJSONStoreEnabled(true)
			.setIncludeEntitiesEnabled(true);

		stream = new TwitterStreamFactory(cb.build()).getInstance();
		
		logger = LoggerFactory.getLogger(TwitterListener.class);
	}

	@Override
	public void start() {

	    final ChannelProcessor channel = getChannelProcessor();
	    final Map<String, String> headers = new HashMap<String, String>();

	    StatusListener listener = new StatusListener() {

		    	public void onStatus(Status status) {
	
		    		logger.info("BARNOWL-SPEED: Stored tweet in temporarySource: " + status.getGeoLocation().toString() );
		    		
		        headers.put("timestamp", String.valueOf(status.getCreatedAt().getTime()));
		        Event event = EventBuilder.withBody(DataObjectFactory.getRawJSON(status).getBytes(), headers);
	
		        channel.processEvent(event);
		      }

		    	public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
		    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
		    public void onScrubGeo(long userId, long upToStatusId) {}
		    public void onException(Exception ex) {}
		    public void onStallWarning(StallWarning warning) {}
	      
	    };

	    stream.addListener(listener);

	    FilterQuery query = new FilterQuery();
		double[][] bb= TwitterListenerUtils.urbanAreas();
		query.locations(bb);
		
		stream.filter(query);

		logger.debug("BARNOWL-SPEED: Twitter stream opened");

		super.start();
	}

	@Override
	public void stop() {
		
		stream.shutdown();
		
		logger.debug("BARNOWL-SPEED: Twitter stream closed");
		
		super.stop();
	}
}