package clast.barnowl.controller.dto;

public class KeywordDto {

	private String keyword;
	private int occurrency;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getOccurrency() {
		return occurrency;
	}

	public void setOccurrency(int occurrency) {
		this.occurrency = occurrency;
	}

}
