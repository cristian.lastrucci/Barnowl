package clast.barnowl.controller.dto;

import java.util.List;

import clast.barnowl.model.City;

public class CityStatisticsDto {

	private City city;
	private List<KeywordDto> keywords;

	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public List<KeywordDto> getKeywords() {
		return keywords;
	}
	public void setKeywords(List<KeywordDto> keywords) {
		this.keywords = keywords;
	}
	
	
}
