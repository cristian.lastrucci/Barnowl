package clast.barnowl.controller.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import clast.barnowl.model.Keyword;

public class KeywordDao {
	
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public KeywordDao() {
		emf = Persistence.createEntityManagerFactory("hbase_pu");
		em = emf.createEntityManager();
	}
	
	public Keyword find (String city, String keyword) {
		
		Query q = em.createQuery("select k from Keyword k where k.city = :city and k.keyword = :keyword");
		q.setParameter("city", city);
		q.setParameter("keyword", keyword);
		
		try {
			Keyword result = (Keyword) q.getSingleResult();
			return result;	
		}catch (Exception e) {
			return null;
		}
		
	}

}
