package clast.barnowl.controller.rs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("rest")
public class Application extends javax.ws.rs.core.Application {

	private Set<Object> singletons = new HashSet<Object>();

	public Application () {
	    singletons.add( new BarnowlRS() );
	}

	@Override
	public Set<Object> getSingletons() {
	    return singletons;
	}
}