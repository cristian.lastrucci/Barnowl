package clast.barnowl.controller.rs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import clast.barnowl.controller.dao.CityStatisticsDao;
import clast.barnowl.controller.dto.CityStatisticsDto;
import clast.barnowl.controller.dto.StatisticsDto;

@Path("/tweets")
public class BarnowlRS {
	
	private CityStatisticsDao statisticsDao;
	
	public BarnowlRS() {
		statisticsDao = new CityStatisticsDao();
	}
	
	@GET
	@Path("helloworld")
	@Produces("text/html")
	public Response getHelloWorld()
	{
		String output = "<h1>Hello World!<h1> <p>RESTful Service is running ...</p>";
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("city-statistics")
	@Produces(MediaType.APPLICATION_JSON)
	public CityStatisticsDto getCityStatistics( @QueryParam("city") String city, @QueryParam("hashtagFilter") boolean hashtagFilter )
	{
		return statisticsDao.find(city, hashtagFilter);
	}
	
	@GET
	@Path("statistics")
	@Produces(MediaType.APPLICATION_JSON)
	public StatisticsDto getAllCitiesStatistics( @QueryParam("hashtagFilter") boolean hashtagFilter )
	{
		return statisticsDao.find(hashtagFilter);
	}
}
