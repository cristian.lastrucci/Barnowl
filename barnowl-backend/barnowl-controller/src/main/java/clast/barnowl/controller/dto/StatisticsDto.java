package clast.barnowl.controller.dto;

import java.util.List;

public class StatisticsDto {
	
	private List<CityStatisticsDto> citiesStatistics;

	public List<CityStatisticsDto> getCitiesStatistics() {
		return citiesStatistics;
	}

	public void setCitiesStatistics(List<CityStatisticsDto> citiesStatistics) {
		this.citiesStatistics = citiesStatistics;
	}
	
	
}
