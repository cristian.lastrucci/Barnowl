package clast.barnowl.controller.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import clast.barnowl.controller.dto.CityStatisticsDto;
import clast.barnowl.controller.dto.KeywordDto;
import clast.barnowl.controller.dto.StatisticsDto;
import clast.barnowl.model.Batch1Keyword;
import clast.barnowl.model.Batch2Keyword;
import clast.barnowl.model.City;
import clast.barnowl.model.SpeedNewKeyword;
import clast.barnowl.model.SpeedOldKeyword;
import clast.barnowl.model.Utils;

public class CityStatisticsDao {
	
	private static Logger logger = LoggerFactory.getLogger(CityStatisticsDao.class);
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("hbase_pu");
	private static EntityManager em = emf.createEntityManager();
	
	public StatisticsDto find(boolean hashtagFilter) {
		
		StatisticsDto statisticsDto = new StatisticsDto();
		List<CityStatisticsDto> citiesStats = new ArrayList<>();
		
		for( City city : City.values() ) {
			citiesStats.add( find(city.toString(), hashtagFilter) );
		}
		
		statisticsDto.setCitiesStatistics(citiesStats);
		return statisticsDto;
	}
	
	public CityStatisticsDto find(String city, boolean hashtagFilter) {
		
		CityStatisticsDto dto = new CityStatisticsDto();
		dto.setCity( City.valueOf(city) );
		
		List<KeywordDto> keywords;
		
		try {
			
			Map<String, KeywordDto> keywordsMap = findBatchKeywords(city, hashtagFilter);
			findSpeedOldKeywords(city, keywordsMap);
			findSpeedNewKeywords(city, keywordsMap);
			
			keywords = new ArrayList<>( keywordsMap.values() );
			
			Collections.sort(keywords, Collections.reverseOrder( (o1, o2) -> o1.getOccurrency() - o2.getOccurrency() ));
			if( keywords.size() > 25) {
				keywords = keywords.subList(0, 25);
			}
			
		}catch (Exception e) {
			logger.error( "BARNOWL-CONTROLLER: Error during " + city + " keywords recovering" + e);
			keywords = new ArrayList<KeywordDto>();
		}
		
		dto.setKeywords(keywords);
		
		return dto;
	}

	private Map<String, KeywordDto> findBatchKeywords(String city, boolean hashtagFilter) {
		
		String queryableBatchTable = findCurrentQueryableBatchTable();
		Map<String, KeywordDto> result = new HashMap<>();
		
		Query q;
		if(hashtagFilter) {
			q = em.createQuery("select k from " + queryableBatchTable + " k where k.city = :city and k.keyword like '#%'");			
		}else {
			q = em.createQuery("select k from " + queryableBatchTable + " k where k.city = :city");			
		}
		
		q.setParameter("city", city);
		q.setMaxResults(100000);
	
		if( "Batch1Keyword".equals(queryableBatchTable) ) {
			List<Batch1Keyword> batch1Keywords = q.getResultList();
			for(Batch1Keyword k : batch1Keywords) {
				KeywordDto kwDto = new KeywordDto();
				kwDto.setKeyword( k.getKeyword() );
				kwDto.setOccurrency( k.getOccurrency() );
				result.put(k.getKeyword(), kwDto);
			}
		}else {
			List<Batch2Keyword> batch2Keywords = q.getResultList();
			for(Batch2Keyword k : batch2Keywords) {
				KeywordDto kwDto = new KeywordDto();
				kwDto.setKeyword( k.getKeyword() );
				kwDto.setOccurrency( k.getOccurrency() );
				result.put(k.getKeyword(), kwDto);
			}
		}
		return result;
	}

	private String findCurrentQueryableBatchTable() {
			
		Utils utils = (Utils) em.createQuery("select u from Utils u").getSingleResult();
		if ( "batch1".equals(utils.getCurrenBatchTable()) ){
			return "Batch1Keyword";
		}else {
			return "Batch2Keyword";
		}
	}
	
	private void findSpeedOldKeywords(String city, Map<String, KeywordDto> keywords) {
		Query q = em.createQuery("select k from SpeedOldKeyword k where k.city = :city");
		q.setParameter("city", city);
		q.setMaxResults(100000);
		List<SpeedOldKeyword> result = q.getResultList();
		for(SpeedOldKeyword k : result) {
			if( keywords.containsKey(k.getKeyword()) ) {
				keywords.get(k.getKeyword()).setOccurrency(keywords.get(k.getKeyword()).getOccurrency() + k.getOccurrency());
			}else {
				KeywordDto kwDto = new KeywordDto();
				kwDto.setKeyword( k.getKeyword() );
				kwDto.setOccurrency( k.getOccurrency() );
				keywords.put(k.getKeyword(), kwDto);
			}
		}	
	}
	
	private void findSpeedNewKeywords(String city, Map<String, KeywordDto> keywords) {
		Query q = em.createQuery("select k from SpeedNewKeyword k where k.city = :city");
		q.setParameter("city", city);
		q.setMaxResults(100000);
		List<SpeedNewKeyword> result = q.getResultList();
		for(SpeedNewKeyword k : result) {
			if( keywords.containsKey(k.getKeyword()) ) {
				keywords.get(k.getKeyword()).setOccurrency(keywords.get(k.getKeyword()).getOccurrency() + 1);
			}else {
				KeywordDto kwDto = new KeywordDto();
				kwDto.setKeyword( k.getKeyword() );
				kwDto.setOccurrency( k.getOccurrency() );
				keywords.put(k.getKeyword(), kwDto);
			}
		}	
	}
}
