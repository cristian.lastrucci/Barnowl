package clast.barnowl.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KeywordKunderaTest {

	private static final String SCHEMA = "hbaseDB";
	private static final String HBASE_PU = "hbase_pu";
	
	private static final UUID uuid1 = UUID.randomUUID();
	private static final UUID uuid2 = UUID.randomUUID();
	private static final UUID uuid3 = UUID.randomUUID();
	private static final UUID uuid4 = UUID.randomUUID();
	private static final UUID uuid5 = UUID.randomUUID();
	private static final UUID uuid6 = UUID.randomUUID();

	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory(HBASE_PU);
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		persistKeywords();
	}

	@Test
	public void testSelectAll() {
		List<Keyword> results = em.createQuery("select k from Keyword k").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(4, results.size());
		Assert.assertTrue(Keyword.class.isAssignableFrom(results.get(0).getClass()));
		assertResults(results, true, true, true, true);
	}

	@Test
	public void testSelectOnId() {
		Query q = em.createQuery("select k from Keyword k where k.id= :uuid");
		q.setParameter("uuid", uuid1);
		List<Keyword> results = q.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		validateKeyword1(results.get(0));

		q = em.createQuery("select k from Keyword k where k.id <> :uuid");
		q.setParameter("uuid", uuid1);
		results = q.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(3, results.size());
		assertResults(results, false, true, true, true);

		List<UUID> uuidList = new ArrayList<UUID>();
		uuidList.add(uuid3);
		uuidList.add(uuid4);
		uuidList.add(uuid5);
		uuidList.add(uuid6);
		q = em.createQuery("select k from Keyword k where k.id in :uuidList");
		q.setParameter("uuidList", uuidList);
		results = q.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		assertResults(results, false, false, true, true);

	}

	@Test
	public void testSelectWithWhereClause() {
		/*------queries with where clause------*/
		List<Keyword> results = em.createQuery("select k from Keyword k where k.keyword = 'keyword1'").getResultList();
		Assert.assertNotNull(results);
		validateKeyword1(results.get(0));

		results = em.createQuery("select k from Keyword k where k.city = 'NEW_YORK'").getResultList();
		Assert.assertNotNull(results);
		validateKeyword3(results.get(0));

		results = em.createQuery("select k from Keyword k where k.city = 'MILAN'").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(3, results.size());
		assertResults(results, true, true, false, true);

		/*------queries with where clause and comparison operators------*/
		results = em.createQuery("select k from Keyword k where k.occurrency > 2").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(3, results.size());
		assertResults(results, true, true, true, false);

		results = em.createQuery("select k from Keyword k where k.occurrency >= 1").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(4, results.size());
		assertResults(results, true, true, true, true);

		results = em.createQuery("select k from Keyword k where k.occurrency < 5").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		assertResults(results, true, false, false, true);

		results = em.createQuery("select k from Keyword k where k.occurrency <= 20").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(4, results.size());
		assertResults(results, true, true, true, true);

		results = em.createQuery("select k from Keyword k where k.occurrency = 8").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		validateKeyword3(results.get(0));

		results = em.createQuery("select k from Keyword k where k.occurrency <> 8").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(3, results.size());
		assertResults(results, true, true, false, true);

		/*------queries with where clause, comparison operators and logical operators------*/
		results = em.createQuery("select k from Keyword k where k.city = 'MILAN' and k.occurrency > 5").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		validateKeyword2(results.get(0));
		
		results = em.createQuery("select k from Keyword k where k.city = 'MILAN' and k.occurrency <> 3 and k.occurrency < 10").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		validateKeyword4(results.get(0));

		results = em.createQuery("select k from Keyword k where k.occurrency = 8 or k.occurrency <= 0 or k.keyword = 'keyword2'").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		assertResults(results, false, true, true, false);

	}

	@Test
	public void testSelectWithInClause() {
		List<Keyword> results = em.createQuery("select k from Keyword k where k.keyword in ('keyword1','keyword2')").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		assertResults(results, true, true, false, false);

		results = em.createQuery("select k from Keyword k where k.occurrency in (2,3)").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		assertResults(results, true, false, false, false);

	}

	@Test
	public void testSelectFields() {
		List results = em.createQuery("select k.keyword from Keyword k").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(4, results.size());
		Collections.sort(results);
		Assert.assertEquals("keyword1", results.get(0));
		Assert.assertEquals("keyword2", results.get(1));
		Assert.assertEquals("keyword3", results.get(2));
		Assert.assertEquals("keyword4", results.get(3));

		Query q = em.createQuery("select k.city from Keyword k where k.id=:uuid");
		q.setParameter("uuid", uuid1);
		results = q.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		Assert.assertEquals(City.MILAN, results.get(0));

		results = em.createQuery("select k.occurrency from Keyword k where k.occurrency > 5").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		Collections.sort(results);
		Assert.assertEquals(8, results.get(0));
		Assert.assertEquals(14, results.get(1));

		results = em.createQuery("select k.keyword, k.occurrency from Keyword k where k.occurrency <> 3").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(3, results.size());
		for (Object result : results) {
			if ("keyword2".equals(((List) result).get(0))) {
				Assert.assertEquals(14, ((List) result).get(1));
			} else if ("keyword3".equals(((List) result).get(0))) {
				Assert.assertEquals(8, ((List) result).get(1));
			} else if ("keyword4".equals(((List) result).get(0))) {
				Assert.assertEquals(1, ((List) result).get(1));
			}
		}

		results = em.createQuery("select k.keyword , k.occurrency from Keyword k where k.city = 'MILAN' and k.occurrency > 10").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		Assert.assertEquals("keyword2", ((List) results.get(0)).get(0));
		Assert.assertEquals(14, ((List) results.get(0)).get(1));

		results = em.createQuery("select k.city, k.occurrency from Keyword k where k.city = 'MILAN' and k.occurrency <> 3 and k.occurrency < 10").getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		Assert.assertEquals(City.MILAN, ((List) results.get(0)).get(0));
		Assert.assertEquals(1, ((List) results.get(0)).get(1));

		List<UUID> uuidList = new ArrayList<UUID>();
		uuidList.add(uuid3);
		uuidList.add(uuid4);
		uuidList.add(uuid5);
		uuidList.add(uuid6);
		q = em.createQuery("select k.keyword from Keyword k where k.id in :uuidList");
		q.setParameter("uuidList", uuidList);
		results = q.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(2, results.size());
		Collections.sort(results);
		Assert.assertEquals("keyword3", results.get(0));
		Assert.assertEquals("keyword4", results.get(1));

		results = em.createQuery("select k.city, k.occurrency from Keyword k where k.keyword in ('keyword1','keyword5','keyword6')")
				.getResultList();
		Assert.assertNotNull(results);
		Assert.assertEquals(1, results.size());
		Assert.assertEquals(City.MILAN, ((List) results.get(0)).get(0));
		Assert.assertEquals(3, ((List) results.get(0)).get(1));
	}

	@Test
	public void testDeleteAll() {
		persistKeywords();
		int result = em.createQuery("delete from Keyword k").executeUpdate();
		Assert.assertEquals(4, result);
		assertDeleted(true, true, true, true);
	}

	@Test
	public void testDeleteOnId() {
		persistKeywords();
		Query q = em.createQuery("delete from Keyword k where k.id = :uuid");
		q.setParameter("uuid", uuid1);
		int result = q.executeUpdate();
		Assert.assertEquals(1, result);
		assertDeleted(true, false, false, false);

		persistKeywords();
		q = em.createQuery("delete from Keyword k where k.id <> :uuid");
		q.setParameter("uuid", uuid1);
		result = q.executeUpdate();
		Assert.assertEquals(3, result);
		assertDeleted(false, true, true, true);

		persistKeywords();
		List<UUID> uuidList = new ArrayList<UUID>();
		uuidList.add(uuid2);
		uuidList.add(uuid3);
		uuidList.add(uuid4);
		uuidList.add(uuid5);
		q = em.createQuery("delete from Keyword k where k.id in :uuidList");
		q.setParameter("uuidList", uuidList);
		result = q.executeUpdate();
		Assert.assertEquals(3, result);
		assertDeleted(false, true, true, true);

	}

	@Test
	public void testDeleteWithWhereClause() {
		persistKeywords();
		int result = em.createQuery("delete from Keyword k where k.keyword = 'keyword1'").executeUpdate();
		Assert.assertEquals(1, result);
		assertDeleted(true, false, false, false);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.city = 'MILAN'").executeUpdate();
		Assert.assertEquals(3, result);
		assertDeleted(true, true, false, true);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.occurrency <= 20").executeUpdate();
		Assert.assertNotNull(result);
		Assert.assertEquals(4, result);
		assertDeleted(true, true, true, true);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.occurrency = 14").executeUpdate();
		Assert.assertEquals(1, result);
		assertDeleted(false, true, false, false);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.occurrency <> 8").executeUpdate();
		Assert.assertNotNull(result);
		Assert.assertEquals(3, result);
		assertDeleted(true, true, false, true);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.city = 'MILAN' and k.occurrency <> 3 and k.occurrency < 10").executeUpdate();
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result);
		assertDeleted(false, false, false, true);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.occurrency = 8 or k.occurrency <= 2 or k.keyword = 'keyword2'").executeUpdate();
		Assert.assertNotNull(result);
		Assert.assertEquals(3, result);
		assertDeleted(false, true, true, true);

	}

	@Test
	public void testDeleteWithInClause() {
		persistKeywords();
		int result = em.createQuery("delete from Keyword k where k.keyword in ('keyword1','keyword2')").executeUpdate();
		Assert.assertEquals(2, result);
		assertDeleted(true, true, false, false);

		persistKeywords();
		result = em.createQuery("delete from Keyword k where k.occurrency in (8,14)").executeUpdate();
		Assert.assertEquals(2, result);
		assertDeleted(false, true, true, false);

	}

	@Test
	public void testUpdateAll() {
		persistKeywords();
		int result = em.createQuery("update Keyword k set k.keyword = 'keyword'").executeUpdate();
		Assert.assertEquals(4, result);
		em.clear();
		Keyword keyword1 = em.find(Keyword.class, uuid1);
		Keyword keyword2 = em.find(Keyword.class, uuid2);
		Keyword keyword3 = em.find(Keyword.class, uuid3);
		Keyword keyword4 = em.find(Keyword.class, uuid4);
		Assert.assertEquals("keyword", keyword1.getKeyword());
		Assert.assertEquals("keyword", keyword2.getKeyword());
		Assert.assertEquals("keyword", keyword3.getKeyword());
		Assert.assertEquals("keyword", keyword4.getKeyword());

		persistKeywords();
		result = em.createQuery("update Keyword k set k.occurrency = 5").executeUpdate();
		Assert.assertEquals(4, result);
		em.clear();
		keyword1 = em.find(Keyword.class, uuid1);
		keyword2 = em.find(Keyword.class, uuid2);
		keyword3 = em.find(Keyword.class, uuid3);
		keyword4 = em.find(Keyword.class, uuid4);
		Assert.assertEquals(5, keyword1.getOccurrency());
		Assert.assertEquals(5, keyword2.getOccurrency());
		Assert.assertEquals(5, keyword3.getOccurrency());
		Assert.assertEquals(5, keyword4.getOccurrency());
	}

	@Test
	public void testUpdateOnId() {
		persistKeywords();
		Query q = em.createQuery("update Keyword k set k.occurrency = 5 where k.id = :uuid");
		q.setParameter("uuid", uuid1);
		int result = q.executeUpdate();
		Assert.assertEquals(1, result);
		em.clear();
		Keyword keyword1 = em.find(Keyword.class, uuid1);
		Assert.assertEquals(5, keyword1.getOccurrency());
		Assert.assertEquals("keyword1", keyword1.getKeyword());

		persistKeywords();
		q = em.createQuery("update Keyword k set k.occurrency = 5 where k.id <> :uuid");
		q.setParameter("uuid", uuid1);
		result = q.executeUpdate();
		Assert.assertEquals(3, result);
		em.clear();
		Keyword keyword2 = em.find(Keyword.class, uuid2);
		Assert.assertEquals(5, keyword2.getOccurrency());
		Assert.assertEquals("keyword2", keyword2.getKeyword());

		Keyword keyword3 = em.find(Keyword.class, uuid3);
		Assert.assertEquals(5, keyword3.getOccurrency());
		Assert.assertEquals("keyword3", keyword3.getKeyword());

		Keyword keyword4 = em.find(Keyword.class, uuid4);
		Assert.assertEquals(5, keyword4.getOccurrency());
		Assert.assertEquals("keyword4", keyword4.getKeyword());

		persistKeywords();
		List<UUID> uuidList = new ArrayList<>();
		uuidList.add(uuid2);
		uuidList.add(uuid3);
		uuidList.add(uuid5);
		q = em.createQuery("update Keyword k set k.city = :newCity where k.id in :uuidList");
		q.setParameter("newCity", City.LOS_ANGELES);
		q.setParameter("uuidList", uuidList);
		result = q.executeUpdate();
		Assert.assertEquals(2, result);
		em.clear();
		keyword2 = em.find(Keyword.class, uuid2);
		keyword3 = em.find(Keyword.class, uuid3);
		Assert.assertEquals(City.LOS_ANGELES, keyword2.getCity());
		Assert.assertEquals(City.LOS_ANGELES, keyword3.getCity());
	}

	@Test
	public void testUpdateWithWhereClause() {
		persistKeywords();
		int result = em.createQuery("update Keyword k set k.occurrency = 6 where k.keyword = 'keyword1'").executeUpdate();
		Assert.assertEquals(1, result);
		em.clear();
		Keyword keyword1 = em.find(Keyword.class, uuid1);
		Assert.assertEquals(6, keyword1.getOccurrency());
		Assert.assertEquals("keyword1", keyword1.getKeyword());

		persistKeywords();
		result = em.createQuery("update Keyword k set k.occurrency = 6, k.keyword='keyword' where k.keyword = 'keyword1'").executeUpdate();
		Assert.assertEquals(1, result);
		em.clear();
		keyword1 = em.find(Keyword.class, uuid1);
		Assert.assertEquals(6, keyword1.getOccurrency());
		Assert.assertEquals("keyword", keyword1.getKeyword());

		persistKeywords();
		Query q = em.createQuery("update Keyword k set k.city = :newCity, k.occurrency = 33 where k.city = :oldCity and k.occurrency <> 3 and k.occurrency < 2");
		q.setParameter("oldCity", City.MILAN);
		q.setParameter("newCity", City.LOS_ANGELES);
		result = q.executeUpdate();
		Assert.assertEquals(1, result);
		em.clear();
		Keyword keyword4 = em.find(Keyword.class, uuid4);
		Assert.assertEquals(33, keyword4.getOccurrency());
		Assert.assertEquals(City.LOS_ANGELES, keyword4.getCity());
	}

	@Test
	public void testUpdateWithInClause() {
		persistKeywords();
		Query q = em.createQuery("update Keyword k set k.city = :newCity where k.keyword in ('keyword1','keyword2')");
		q.setParameter("newCity", City.LOS_ANGELES);
		int result = q.executeUpdate();
		Assert.assertEquals(2, result);
		em.clear();
		Keyword keyword1 = em.find(Keyword.class, uuid1);
		Keyword keyword2 = em.find(Keyword.class, uuid2);
		Assert.assertEquals(City.LOS_ANGELES, keyword1.getCity());
		Assert.assertEquals(City.LOS_ANGELES, keyword2.getCity());

		persistKeywords();
		result = em.createQuery("update Keyword k set k.keyword = 'keyword', k.occurrency = 44 where k.occurrency in (8,14)").executeUpdate();

		Assert.assertEquals(2, result);

		em.clear();
		keyword1 = em.find(Keyword.class, uuid2);
		keyword2 = em.find(Keyword.class, uuid3);
		Assert.assertEquals("keyword", keyword1.getKeyword());
		Assert.assertEquals("keyword", keyword2.getKeyword());
		Assert.assertEquals(44, keyword1.getOccurrency());
		Assert.assertEquals(44, keyword2.getOccurrency());

	}

	@After
	public void tearDown() throws Exception {
		deleteKeywords();
		em.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
		emf = null;
	}
	
	private Keyword prepareData(UUID id, String keyword, City city, int occurrency) {
		Keyword k = new Keyword();
		k.setId(id);
		k.setKeyword(keyword);
		k.setCity(city);
		k.setOccurrency(occurrency);
		return k;
	}

	private void persistKeywords() {
		Keyword kw1 = prepareData(uuid1, "keyword1", City.MILAN, 3);
		Keyword kw2 = prepareData(uuid2, "keyword2", City.MILAN, 14);
		Keyword kw3 = prepareData(uuid3, "keyword3", City.NEW_YORK, 8);
		Keyword kw4 = prepareData(uuid4, "keyword4", City.MILAN, 1);
		em.persist(kw1);
		em.persist(kw2);
		em.persist(kw3);
		em.persist(kw4);
		em.clear();
	}

	private void deleteKeywords() {
		em.createQuery("delete from Keyword k").executeUpdate();
		em.clear();
	}

	private void validateKeyword1(Keyword keyword) {
		Assert.assertNotNull(keyword);
		Assert.assertEquals(uuid1, keyword.getId());
		Assert.assertEquals("keyword1", keyword.getKeyword());
		Assert.assertEquals(City.MILAN, keyword.getCity());
		Assert.assertEquals(3, keyword.getOccurrency());
	}

	private void validateKeyword2(Keyword keyword) {
		Assert.assertNotNull(keyword);
		Assert.assertEquals(uuid2, keyword.getId());
		Assert.assertEquals("keyword2", keyword.getKeyword());
		Assert.assertEquals(City.MILAN, keyword.getCity());
		Assert.assertEquals(14, keyword.getOccurrency());
	}

	private void validateKeyword3(Keyword keyword) {
		Assert.assertNotNull(keyword);
		Assert.assertEquals(uuid3, keyword.getId());
		Assert.assertEquals("keyword3", keyword.getKeyword());
		Assert.assertEquals(City.NEW_YORK, keyword.getCity());
		Assert.assertEquals(8, keyword.getOccurrency());
	}

	private void validateKeyword4(Keyword keyword) {
		Assert.assertNotNull(keyword);
		Assert.assertEquals(uuid4, keyword.getId());
		Assert.assertEquals("keyword4", keyword.getKeyword());
		Assert.assertEquals(City.MILAN, keyword.getCity());
		Assert.assertEquals(1, keyword.getOccurrency());
	}

	private void assertResults(List<Keyword> results, boolean foundKw1, boolean foundKw2, boolean foundKw3, boolean foundKw4) {
		for (Keyword k : results) {
			if(uuid1.equals( k.getId() ) ) {
				if (foundKw1) {
					validateKeyword1(k);
				}else {
					Assert.fail();					
				}
			}else if(uuid2.equals( k.getId() ) ) {
				if (foundKw2) {
					validateKeyword2(k);
				}else {
					Assert.fail();					
				}
			}else if(uuid3.equals( k.getId() ) ) {
				if (foundKw3) {
					validateKeyword3(k);
				}else {
					Assert.fail();					
				}
			}else if(uuid4.equals( k.getId() ) ) {
				if (foundKw4) {
					validateKeyword4(k);
				}else {
					Assert.fail();					
				}
			}
		}
	}

	private void assertDeleted(boolean foundKw1, boolean foundKw2, boolean foundKw3, boolean foundKw4) {
		Keyword kw1 = em.find(Keyword.class, uuid1);
		Keyword kw2 = em.find(Keyword.class, uuid2);
		Keyword kw3 = em.find(Keyword.class, uuid3);
		Keyword kw4 = em.find(Keyword.class, uuid4);
		if (foundKw1) {
			Assert.assertNull(kw1);
		} else {
			Assert.assertNotNull(kw1);
		}
		if (foundKw2) {
			Assert.assertNull(kw2);
		} else {
			Assert.assertNotNull(kw2);
		}
		if (foundKw3) {
			Assert.assertNull(kw3);
		} else {
			Assert.assertNotNull(kw3);
		}
		if (foundKw4) {
			Assert.assertNull(kw4);
		} else {
			Assert.assertNotNull(kw4);
		}
	}
}
