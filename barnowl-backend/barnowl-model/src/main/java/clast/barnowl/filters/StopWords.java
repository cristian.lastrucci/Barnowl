package clast.barnowl.filters;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum StopWords {
	ON, IN, AT, SINCE, FOR, AGO, BEFORE, TO, PAST, TILL, UNTIL, BY, NEXT, BESIDE, UNDER, BELOW, OVER,
	ACROSS, THROUGH, INTO, TOWARDS, ONTO, FROM, OF, OFF, OUT, ABOUT, NEAR, ABOVE, BETWEEN, BEYOND,
	AFTER, AGAINST, DESPITE, UNLIKE, ALONG, DOWN, OPPOSITE, AMONG, DURING, UP, AROUND, EXCEPT,OUTSIDE,
	UPON, AS, VIA, WITH, ROUND, WITHIN, BEHIND, INSIDE, WITHOUT, THAN, LIKE, THE, AN, THIS, THESE, THAT,
	THOSE, MY, MINE, YOUR, YOURS, HIS, HER, HERS, ITS, OUR, OURS, THEIR, THEIRS, AND, OR, IF, WHY,
	BECAUSE, IS, ARE, WAS, HAVE, HAD, BE, BEEN, HAS, YOU, HE, SHE, IT, WE, THEY, ABLE, BAD, BEST,
	BETTER, 	BIG, BLACK, CERTAIN, CLEAR, DIFFERENT, EARLY, EASY, ECONOMIC, EMPTY, FEDERAL, FREE, FULL,
	GOOD, GLOBAL, GREAT, HARD, HIGH, HUMAN, IMPORTANT, INTERNATIONAL, LARGE, LATE, LITTLE, LOCAL, LONG,
	LOW, MAJOR, MILITARY, NATIONAL, NEW, OLD, ONLY, OTHER, POLITICAL, POSSIBLE, IMPOSSIBLE, PUBLIC,
	PRIVATE, REAL, RECENT, SMALL, SOCIAL, SURE, TRUE, FALSE, WHITE, WHOLE, YOUNG, RED, GREEN, BLUE,
	YELLOW, GRAY, JUST, NOW, LATEST, CAN, COULD, WOULD, WANT, SEE, SO, ALL, ME, ONE, TWO, THREE, FOUR,
	FIVE, SIX, SEVEN, EIGHT, NINE, TEN, VERY, BACK, GREATER, CLICK, CHECK, BUT,
	
	DI, A, DA, CON, SU, PER, TRA, FRA, IL, LO, LA, GLI, LE, UN, UNO, UNA, DEL, AL, DAL, NEL, SUL, DELLO,
	ALLO, DALLO, NELLO, SULLO, DELLA, ALLA, DALLA, NELLA, SULLA, DEI, AI, DAI, NEI, SUI, DEGLI, DAGLI,
	NEGLI, SUGLI, DELLE, ALLE, DALLE, NELLE, SULLE, CHE, MIO, MIA, TUO, TUA, MIEI, TUOI, IO, MI, TU, TE,
	TI, LUI, LEI, ELLA, ELLO, NOI, CI, VOI, VI, LORO, CATTIVO, MIGLIORE, MEGLIO, GRANDE, GROSSO, NERO,
	CERTO, SICURO, PULITO, CHIARO, DIVERSO, DIFFERENTE, GIOVANE, NUOVO, NUOVA, FACILE, ECONOMICO,
	ECONOMICA, ECONOMICI, ECONOMICHE, VUOTO, VUOTA, VUOTI, VUOTE, STATALE, GRATIS, LIBERO, LIBERA,
	LIBERI, LIBERE, PIENO, PIENA, PIENI, PIENE, BUONO, BUONA, BUONI, BUONE, GLOBALE, GLOBALI, DURO, DURA,
	DURI, DURE, ALTO, ALTA, ALTI, ALTE, BASSO, BASSA, BASSI, BASSE, IMPORTANTE, INTERNAZIONALE, INTERNAZIONALI,
	LARGO, LARGA, LARGHI, LARGHE, ULTIMO, ULTIMA, ULTIMI, ULTIME, PICCOLO, PICCOLA, PICCOLI, PICCOLE,
	LOCALE, LOCALI, LUNGO, LUNGA, LUNGHI, LUNGHE, VECCHIO, VECCHIA, VECCHI, VECCHIE, GIOVANI, ALTRO, ALTRA,
	ALTRI, ALTRE, POSSIBILE, IMPOSSIBILE, PUBBLICO, PUBBLICA, PUBBLICI, PUBBLICHE, PRIVATO, PRIVATA,
	PRIVATI, REALE, REALI, RECENTE, RECENTI, SOCIALE, SOCIALI, SICURA, SICURI, SICURE, VERO, FALSO, BIANCO,
	INTERO, INTERA, INTERI, INTERE, ROSSO, VERDE, BLU, GIALLO, GRIGIO, ADESSO, POSSO, PUOI, VOGLIO, VUOI,
	VORREI, VORRESTI, VEDI, VEDO, QUINDI, ANCHE, ALTRIMENTI, MA, SE, ALLORA;
	
	public static boolean isStopWord(String word) {
		
		if( !word.matches("^[a-zA-Z0-9_#]*$") || word.length() < 2 ) {
			return true;
		}
		
		return Arrays.asList(StopWords.values()).stream()
													.map( sw -> sw.toString() )
													.collect(Collectors.toList())
														.contains(word.toUpperCase());
	}
}
