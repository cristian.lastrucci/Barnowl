package clast.barnowl.model;

import java.util.HashMap;
import java.util.Map;

public enum City {
	
	LOS_ANGELES, NEW_YORK, LONDON, MILAN, SYDNEY;
	
	private static Map<City, Coordinates> citiesCoords;
	
	static {
		citiesCoords = new HashMap<City, Coordinates>();
		citiesCoords.put(LOS_ANGELES, new Coordinates(34.052234, -118.243685));
		citiesCoords.put(NEW_YORK, new Coordinates(40.712775, -74.005973));
		citiesCoords.put(LONDON, new Coordinates(51.507351, -0.127758));
		citiesCoords.put(MILAN, new Coordinates(45.458626, 9.181873));
		citiesCoords.put(SYDNEY, new Coordinates(-33.86882, 151.209296));
	}
	
	public static Coordinates getCityCoordinates(City city) {
		return citiesCoords.get(city);
	}
}
