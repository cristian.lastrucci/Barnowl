package clast.barnowl.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "keywords", schema = "hbaseDB@hbase_pu")
public class Keyword {

	@Id
	@Column(name = "id")
	private UUID id;

	@Column(name = "keyword")
	private String keyword;

	@Column(name = "city")
	@Enumerated(EnumType.STRING)
	private City city;

	@Column(name = "occurrency")
	private int occurrency;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public int getOccurrency() {
		return occurrency;
	}

	public void setOccurrency(int occurrency) {
		this.occurrency = occurrency;
	}

}
