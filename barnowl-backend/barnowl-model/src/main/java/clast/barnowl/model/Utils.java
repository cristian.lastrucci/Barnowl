package clast.barnowl.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utils", schema = "hbaseDB@hbase_pu")
public class Utils {

	@Id
	@Column(name = "id")
	private UUID id;

	@Column(name = "currenBatchTable")
	private String currenBatchTable;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCurrenBatchTable() {
		return currenBatchTable;
	}

	public void setCurrenBatchTable(String currenBatchTable) {
		this.currenBatchTable = currenBatchTable;
	}

}