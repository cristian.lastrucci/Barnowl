package clast.barnowl.model;

public class BoundingBox {

	private Coordinates ne;
	private Coordinates sw;

	public BoundingBox(Coordinates ne, Coordinates sw) {
		this.ne = ne;
		this.sw = sw;
	}

	public Coordinates getNe() {
		return ne;
	}

	public Coordinates getSw() {
		return sw;
	}
	
	public boolean checkMembership(Coordinates coords) {
		
		if(coords == null || coords.getLatitude() == null || coords.getLongitude() == null) {
			return false;
		}
		
		if( coords.getLatitude() < sw.getLatitude() ||
				coords.getLongitude() < sw.getLongitude() ||
				coords.getLatitude() > ne.getLatitude() ||
				coords.getLongitude() > ne.getLongitude()) {
			return false;
		}

		return true;
	}

}
