package clast.barnowl.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UrbanArea {

	private static Map<City, BoundingBox> urbanAreas;
	
	static {
		urbanAreas = new HashMap<City, BoundingBox>();
		urbanAreas.put(City.LOS_ANGELES, new BoundingBox(new Coordinates(34.768691, -117.339478), new Coordinates(33.335118, -118.89679)));
		urbanAreas.put(City.NEW_YORK, new BoundingBox(new Coordinates(41.787697, -72.784424), new Coordinates(40.057052, -75.025635)));
		urbanAreas.put(City.LONDON, new BoundingBox(new Coordinates(51.932412, 0.858307), new Coordinates(51.12766, -0.797882)));
		urbanAreas.put(City.MILAN, new BoundingBox(new Coordinates(45.638527, 9.549866), new Coordinates(45.263772, 8.695679)));
		urbanAreas.put(City.SYDNEY, new BoundingBox(new Coordinates(-33.819089, 151.301651), new Coordinates(-34.092473, 150.977554)));
	}
	
	public static BoundingBox getCityBoundaries(City city) {
		return urbanAreas.get(city);
	}
	
	public static Set<City> getCities(){
		return urbanAreas.keySet();
	}
	
	public static City findCity(Coordinates coords) {
		City result = null;
		List<City> cities = new ArrayList<City>(urbanAreas.keySet());
		for(int i=0; i<cities.size() && result==null; i++) {
			if( urbanAreas.get(cities.get(i)).checkMembership(coords) ) {
				result = cities.get(i);
			}
		}
		return result;
	}

}
