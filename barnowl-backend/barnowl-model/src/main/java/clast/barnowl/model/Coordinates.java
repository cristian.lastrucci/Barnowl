package clast.barnowl.model;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

public class Coordinates {

	private Double latitude;
	private Double longitude;
	
	public Coordinates() {}
	
	public Coordinates(Double latitude, Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}
	
	@Override
	public String toString() {
		return "[" + latitude.toString() + "," + longitude.toString() + "]";
	}
	
	public void initFromJSON(String jsonLocation) {
		try {
			JSONParser jsonParser = new JSONParser(JSONParser.MODE_PERMISSIVE);
			JSONObject location = (JSONObject) jsonParser.parse(jsonLocation);
			String type = (String) location.get("type");
			String coordinates = location.get("coordinates").toString();
			
			
			if( "Point".equals(type) ) {
				parsePointCoordinates(coordinates);
			}else if( "Polygon".equals(type) ) {
				parsePolygonCoordinates(coordinates);
			}else {
				throw new IllegalArgumentException();
			}
			
		}catch (Exception e) {
			throw new IllegalArgumentException("Cannot extract coordinates from: " + jsonLocation);
		}
	}

	private void parsePointCoordinates(String coordinates) {
		String[] coords = coordinates.replace("[", "").replace("]", "").split(",");
		latitude = Double.valueOf(coords[1]);
		longitude = Double.valueOf(coords[0]);
	}
	
	private void parsePolygonCoordinates(String coordinates) {
		String[]coords = coordinates.replace("[[[", "").replace("]]]", "").replace("],[", "&").split("&");
		Double latSum = 0d;
		Double lonSum = 0d;
		for(String c : coords) {
			String[] ll = c.split(",");
			latSum += Double.valueOf(ll[1]);
			lonSum += Double.valueOf(ll[0]);
		}
		latitude = latSum/coords.length;
		longitude = lonSum/coords.length;
	}
}
