import React, { Component } from 'react';
import { StatisticsWrapper } from './components/StatisticsWrapper';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {hashtagFilter: "false"}

    this.handler = this.handler.bind(this);
    this.setHashtagFilterState = this.setHashtagFilterState.bind(this);

  }

  handler() {
    if (document.getElementById('lastUpdateTime') !== null) {
      document.getElementById('lastUpdateTime').innerHTML=new Date().toLocaleString();
    }
  }

  setHashtagFilterState(){
    if(this.state.hashtagFilter === "true"){
      this.setState({hashtagFilter: "false"})
    }else{
      this.setState({hashtagFilter: "true"})
    }
  }

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <div className="lastUpdateWrapper">
            <div className="lastUpdateText">
              <span className="lastUpdateLabel">Last Update:</span>
              <span className="lastUpdateValue" id="lastUpdateTime">{new Date().toLocaleString()}</span>
            </div>
          </div>
          <div className="titleWrapper">
            <div className="logoWrapper">
              <h1 className="App-title"></h1>
            </div>
          </div>
          <div className="filtersWrapper">
            <form>
                <input
                  name="onlyHashtag"
                  type="checkbox"
                  id="onlyHashtagCB"
                  onChange={this.setHashtagFilterState}/>
                <span className="hashtagFilterLabel">Only Hashtag</span>
            </form>
          </div>
        </header>
        <div className="body">
          <StatisticsWrapper handler={this.handler} hashtagFilter={this.state.hashtagFilter} />
        </div>
      </div>
    );
  }
}

export default App;
