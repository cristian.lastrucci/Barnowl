import React, { Component } from 'react';
import { Word } from './Word';
import './WordList.css';

export class WordList extends Component {

  render() {
    var keywords;
    if(this.props.keywords){
      keywords = this.props.keywords.map( (w, index) =>
                <Word
                  keyword={w.keyword}
                  occurrency={w.occurrency}
                  key={index}
                />
              );
    }else{
      keywords = <Word keyword="" occurrency="" key="1" />
    }

    return (
      <div className="wordList">
        <div className="titlesRow">
          <span className="occurrencyTitle">#</span>
          <span className="wordTitle">Word</span>
        </div>
        <div>
          {keywords}
        </div>
      </div>
    );
  }
}
