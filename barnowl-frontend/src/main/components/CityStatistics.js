import React, { Component } from 'react';
import { WordList } from './WordList';
import './CityStatistics.css';
import 'whatwg-fetch'

export class CityStatistics extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="cityStatsWrapper">
        <div className={this.props.wrapperClasses} >
          <div className="cityTitleWrapper">
            <h3 className="cityTitle">{this.props.city}</h3>
          </div>
          <WordList keywords={this.props.keywords} />
        </div>
      </div>
    );
  }
}
