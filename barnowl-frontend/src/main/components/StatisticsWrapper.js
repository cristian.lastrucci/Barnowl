import React, { Component } from 'react';
import { CityStatistics } from './CityStatistics';
import './StatisticsWrapper.css';
import 'whatwg-fetch'

const URL = "http://localhost:8080/barnowl-controller/rest/tweets/statistics"

export class StatisticsWrapper extends Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.findStatistics()

    setInterval(() => {
      this.findStatistics()
    }, 10000);
  }

  findStatistics(){

    const headers = () => {
      const headers =  new Headers({
        "Content-Type": "application/json"
      })

      return headers
    }

    fetch(URL + "?hashtagFilter=" + this.props.hashtagFilter, { headers: headers() })
    .then(function(response) {
      var contentType = response.headers.get("content-type");
      if(contentType && contentType.includes("application/json")) {
        return response.json();
      }
      throw new TypeError("Oops, we haven't got JSON!");
    })
    .then( json => this.setState({ citiesStats: json.citiesStatistics }) )
    .catch(error => console.log(error) );
  }

  cityStatsWrapperLayout(city){
    var internalWrapperClass = "cityStatsInternalWrapper"
    var cityClass
    switch (city) {
      case "LOS_ANGELES":
        cityClass = "losangelesBackground"
        break;
      case "NEW_YORK":
        cityClass = "newyorkBackground"
        break;
      case "LONDON":
        cityClass = "londonBackground"
        break;
      case "MILAN":
        cityClass = "milanBackground"
        break;
      case "SYDNEY":
        cityClass = "sydneyBackground"
        break;
    }

    return internalWrapperClass + " " + cityClass
  }

  render() {

    this.props.handler()

    var citiesStats = [];
    if(this.state.citiesStats){
      for (var i = 0; i < this.state.citiesStats.length; i++) {
        citiesStats.push( <CityStatistics
                            city={this.state.citiesStats[i].city}
                            keywords={this.state.citiesStats[i].keywords}
                            wrapperClasses={this.cityStatsWrapperLayout(this.state.citiesStats[i].city)}
                            key={i}
                          /> );
      }
    }else{
      citiesStats.push( <div className="statisticsNotFoundWrapper" key={0}>
                          <p>Tweet statistics are not yet available.</p>
                          <p>Wait a few moments...</p>
                        </div> );
    }

    return (
      <div className="statsWrapper">
        {citiesStats}
      </div>
    );
  }
}
