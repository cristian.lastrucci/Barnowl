import React, { Component } from 'react'
import './Word.css';

export class Word extends Component {

  render() {
    return (
      <div className="wordWrapper">
        <span className="occurrencyLabel">{this.props.occurrency}</span>
        <span className="wordLabel">{this.props.keyword}</span>
      </div>
    );
  }
}
